<?php
include './src/get_codes.php'; # $http_codes is now available

# current url
$actual_link = "https://$_SERVER[HTTP_HOST]";

// return text colored in gold
function gold_color(string $text): string
{
	return '<span style="color: gold;">' . $text . '</span>';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>HTTP Status Dragons</title>
	<meta name="description" content="Image of a Dragon for each HTTP Status Code">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/water-dark.css">
	<link rel="stylesheet" href="/assets/styles.css">

	<!-- Include favicon paths -->
	<?php include "./src/include_favicons.php" ?>

	<!-- Disable Dark Reader -->
	<meta name="darkreader-lock">

	<!-- Facebook Meta Tags -->
	<meta property="og:url" content="https://httpdragons.com">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Image of a Dragon for each HTTP Status">
	<meta property="og:description" content="HTTP Status Dragons is where you can upload and see images of a dragon representing every http code!">
	<meta property="og:image" content="https://httpdragons.com/images/example.png">

	<!-- Twitter Meta Tags -->
	<meta name="twitter:card" content="summary_large_image">
	<meta property="twitter:domain" content="httpdragons.com">
	<meta property="twitter:url" content="https://httpdragons.com">
	<meta name="twitter:title" content="Image of a Dragon for each HTTP Status">
	<meta name="twitter:description" content="HTTP Status Dragons is where you can upload and see images of a dragon representing every http code!">
	<meta name="twitter:image" content="https://httpdragons.com/images/example.png">

	<!-- Meta Tags Generated via https://www.opengraph.xyz -->

</head>

<!-- Needs to be defined before html is read -->
<script>
	// image is not found
	function onError(th, code) {
		th.onerror = null;
		th.parentElement.parentElement.outerHTML = `
<div class='not-available'>
  Not Available<br/><a href='/src/upload.php?code=${code}'>Upload a Dragon</a>
</div>
`;
	}
</script>



<body>
	<!--[if lt IE 8]>
			<p class="browserupgrade">
			You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
			</p>
		<![endif]-->

	<!-- Include header -->
	<?php include "./src/header.php" ?>

	<div>
		<h2> Usage </h2>
		<div>Replace
			<?= gold_color('[code]') ?> with the http code.
			<?= gold_color('[width]') ?> and
			<?= gold_color('[height]') ?> to optionally specify dimensions. </div>
		<details>
			<summary>
				<span>
					<?= $actual_link
						. '/'
						. gold_color('[code]')
						. '.png?w='
						. gold_color('[width]')
						. '&h='
						. gold_color('[height]')
					?>
				</span>
			</summary>
			<span>
				Get 404 image at full resolution
				<span id='link-0'>
					<code><?= $actual_link . '/404.png' ?></code>
					<input title='Copy to clipboard' class='clipboard-button-hover clipboard-button' type="image" alt="clipboard button" src="/assets/icons/clipboard.svg" onclick='copyToClipBoard("#link-0")' />
				</span>
			</span>
			<span>
				Specify width, height will adapt automatically
				<span id='link-1'>
					<code><?= $actual_link . '/404.png?w=500' ?></code>
					<input title='Copy to clipboard' class='clipboard-button-hover clipboard-button' type="image" alt="clipboard button" src="/assets/icons/clipboard.svg" onclick='copyToClipBoard("#link-1")' />
				</span>
			</span>
			<span>
				Specify height, width will adapt automatically
				<span id='link-2'>
					<code><?= $actual_link . '/404.png?h=500' ?></code>
					<input title='Copy to clipboard' class='clipboard-button-hover clipboard-button' type="image" alt="clipboard button" src="/assets/icons/clipboard.svg" onclick='copyToClipBoard("#link-2")' />
				</span>
			</span>
			<span>
				Specify height and width
				<span id='link-3'>
					<code><?= $actual_link . '/404.png?w=500&h=200' ?></code>
					<input title='Copy to clipboard' class='clipboard-button-hover clipboard-button' type="image" alt="clipboard button" src="/assets/icons/clipboard.svg" onclick='copyToClipBoard("#link-3")' />
				</span>
			</span>
		</details>

		<details>
			<summary>
				<span>
					<?= $actual_link
						. '/'
						. gold_color('[code]')
						. '.json'
					?>
				</span>
			</summary>
			<span>
				Get 404's description in json format
				<span id='link-4'>
					<code><?= $actual_link . '/404.json' ?></code>
					<input title='Copy to clipboard' class='clipboard-button-hover clipboard-button' type="image" alt="clipboard button" src="/assets/icons/clipboard.svg" onclick='copyToClipBoard("#link-4")' />
				</span>
			</span>
		</details>
	</div>
	<div class="pictures">
		<?php foreach ($http_codes as $code => $elems) : ?>
			<div>
				<a href="<?= '/src/preview_image.php?code=' . $code ?>" style='height: 300px; width: 100%;'>
					<div class='border-hover img-container' style='height: 100%; width: 100%;'>
						<img style='object-fit: cover; height: 100%; width: 100%;' class='img-preview' src='<?= '/' . $code . '.png?h=300' ?>' alt='<?= "HTTP Status Dragon " . $code . ' ' . $elems['http_message'] ?>' onerror='onError(this, <?= $code ?>)' />
					</div>
				</a>
				<div style='margin-top: 20px' class='picture-title'><?= $code ?></div>
				<div><?= $elems['http_message'] ?></div>
			</div>
		<?php endforeach; ?>
	</div>

	<!-- Include footer html -->
	<?php include "./src/footer.php" ?>

</body>

<script type="text/javascript" src="/src/main.js"></script>

</html>
