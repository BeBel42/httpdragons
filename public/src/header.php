<?php
$currentUrl = $_SERVER['REQUEST_URI'];
# /src/waw.php if on httpdragons.com/src/waw.php
$filePath = parse_url($currentUrl, PHP_URL_PATH);
$is_root = $filePath === '/';
?>


<header style='margin-top: 60px'>
	<?php if (!$is_root) : ?>
		<div class='return-home'>
			<a href='/'><?= htmlspecialchars('<') ?> Home</a>
		</div>
	<?php endif; ?>
	<div class='heading'>
		<h1>HTTP Status Dragons</h1>
		<?php if ($is_root) : ?>
			<h2>A dragon for every HyperText Transfer Protocol response status code</h2>
			<div>Inspired from <a href='https://httpcats.com'>HTTP Status Cats</a></div>
		<?php endif; ?>
	</div>
</header>
