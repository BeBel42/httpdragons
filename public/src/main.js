"use strict";

// to prevent spamming
const ref = document.getElementById("put-email");
if (ref)
	ref.innerHTML =
		`<a href='mai` + `lto:cont` + `act@mlefe` + `vre.xyz'>send me an email</a>`;

// notice object for clipboard
function makeNotice() {
	const notice = document.createElement("div");
	notice.innerText = "Copied to clipboard!";
	notice.style["position"] = "absolute";
	notice.style["background-color"] = "#161f27";
	notice.style["padding"] = "10px";
	notice.style["border-radius"] = "5px";
	notice.style["display"] = "none";
	notice.style["box-shadow"] = "4px 4px #11111155";
	notice.id = "notice";
	document.querySelector("body").appendChild(notice);
}
makeNotice();

// copy text to clipboard
function copyToClipBoard(querySelector) {
	const text = document.querySelector(querySelector)?.innerText;
	navigator.clipboard.writeText(text);
	const btn = document.querySelector(`${querySelector} > input`);
	btn.src = "/assets/icons/checked.png";
	btn.classList.remove("clipboard-button-hover");
	btn.classList.add("clipboard-button-clicked");

	// notify user
	const notice = document.getElementById("notice");
	const bounds = btn.getBoundingClientRect();
	const x = bounds.x - 175;
	const y = bounds.y;
	notice.style["left"] = `${x | 0}px`;
	notice.style["top"] = `${(window.scrollY + y) | 0}px`;
	notice.style["display"] = "block";
	setTimeout(() => resetCPIcon(querySelector), 2000);
}

// mouse leaves.
function resetCPIcon(querySelector) {
	const btn = document.querySelector(`${querySelector} > input`);
	if (btn) {
		btn.src = "/assets/icons/clipboard.svg";
		btn.classList.add("clipboard-button-hover");
		btn.classList.remove("clipboard-button-clicked");
	}

	// stop notifying user
	const notice = document.getElementById("notice");
	if (notice) {
		notice.style["display"] = "none";
	}
}
