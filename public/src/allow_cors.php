<?php

/*
  Include this to allow all CORS requests on the endpoint
  Requests will be restricted to GET
  */

// Allow requests from all origins
header('Access-Control-Allow-Origin: *');

// Specify that only GET requests are allowed
header('Access-Control-Allow-Methods: GET');

// Handle preflight (OPTIONS) request
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    // Exit early, preflight requests don't need to return the actual API response
    exit;
}

// Check if the request method is not GET
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    // Send a 405 Method Not Allowed response if the method is not GET
    header('HTTP/1.1 405 Method Not Allowed');
    exit;
}
