<footer style='text-align: center; padding-top: 20px;'>
	<style>
		footer nav a {
			margin-left: 10px;
			margin-right: 10px;
		}
	</style>
	<nav>
		<a href='/src/about.php'>About</a>
		<a href='https://mlefevre.xyz'>Maintainer's homepage</a>
		<a href='https://gitlab.com/BeBel42/httpdragons'>Source Code</a>
		<a href='/'>Home</a>
		<a href='https://gitlab.com/BeBel42/httpdragons/-/blob/main/LICENSE?ref_type=heads'>License</a>
	</nav>

</footer>
