<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>File Upload</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/water-dark.css">
	<link rel="stylesheet" href="/assets/styles.css">
	<script type="text/javascript" src="/src/main.js" defer></script>

	<!-- Include favicon paths -->
	<?php include "./include_favicons.php" ?>

	<!-- Disable Dark Reader -->
	<meta name="darkreader-lock">

</head>

<body>
	<!--[if lt IE 8]>
			<p class="browserupgrade">
			You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
			</p>
		<![endif]-->

	<!-- Include header -->
	<?php include "./header.php" ?>

	<?php
	//Import PHPMailer classes into the global namespace
	//These must be at the top of your script, not inside a function
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	$image = $_FILES['image'];

	// Check if image file is a actual image or fake image
	if (isset($_POST["submit"])) {
		$check = getimagesize($image["tmp_name"]);
		if ($check !== false) {
			echo "File is an image - " . $check["mime"] . ".";
		} else {
			exit("File is not an image.");
		}
	}
	// Allow certain file formats
	$target_file = basename($image["name"]);
	$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
	if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
		exit("Sorry, only JPG, JPEG & PNG files are allowed.");
	}

	//Load Composer's autoloader
	require '../../vendor/autoload.php';

	use Symfony\Component\Dotenv\Dotenv;

	$dotenv = new Dotenv();
	$dotenv->load(__DIR__ . '/../../.env');

	// Retrive env variables
	$userName = $_ENV['SMTP_USER'];
	$userPass = $_ENV['SMTP_PASSWORD'];
	$maintainerMail = $_ENV['MAINTAINER_EMAIL'];
	$maintainerName = $_ENV['MAINTAINER_NAME'];

	//Create an instance; passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {
		//Server settings
		#$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
		$mail->isSMTP();                                            //Send using SMTP
		$mail->Host       = 'smtp.purelymail.com ';                     //Set the SMTP server to send through
		$mail->SMTPAuth   = true;                                   //Enable SMTP authentication
		$mail->Username   = $userName;
		$mail->Password   = $userPass;
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
		$mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

		//Recipients
		$mail->setFrom($userName, 'HTTP Dragon');
		$mail->addAddress($maintainerMail, $maintainerName);     //Add a recipient

		//Attachments
		$mail->addAttachment($image['tmp_name'], $target_file);    //Optional name

		//Content
		$mail->Subject = 'HTTP Dragon for code ' . $_POST['code'];
		$pname = $_POST['name'] ?: '<anonymous>';
		$pemail = $_POST['email'] ?: '<none>';
		$purl = $_POST['url'] ?: '<none>';
		$text = $_POST['text'] ?: '<no text>';
		$mail->Body = "Author: {$pname}\nEmail: {$pemail}\nAuthorUrl: {$purl}\n\n{$text}";

		$mail->send();
	?>
		<h1>Message has been sent:</h1>
		<blockquote>
			<?php
			// formatting output
			$mail_body = '';
			foreach (explode("\n", $mail->Body) as $line) {
				$mail_body .= htmlspecialchars($line);
				$mail_body .= '<br/>';
			}
			// formatting img size
			$img_size = (int)$_FILES['image']['size'] / 1000_000;
			$img_size = number_format($img_size, 2) . 'MB';
			?>
			<h3><?= htmlspecialchars($mail->Subject) ?></h3>
			<div>
				<?= $mail_body ?>
			</div>
			<footer>The image <?= htmlspecialchars($_FILES['image']['name']) ?>
				of size
				<?= $img_size ?>
				was provided</footer>
		</blockquote>
	<?php
	} catch (Exception $e) {
		echo "<div style='
				background-color: #ff000033;
				border: 2px solid #ff888877;
				border-radius: 7px;
				padding: 15px;
				margin-top: 30px;
				margin-bottom: 30px;
				text-align: center;
				'>Message could not be sent. Mailer Error: {$mail->ErrorInfo}</div>";
		echo '<div style="
				text-align: center;
				"
				><b>Having issues?</b> You can always <span id="put-email"></span></div>';
	}

	?>

	<!-- Include footer html -->
	<?php include "./footer.php" ?>

</body>

</html>
