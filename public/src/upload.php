<?php
$code = ((int)$_GET['code'] ?: 404);
$max_size = 5; // MB
$no_file = 'No file selected';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Upload - HTTP Status Dragons</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/water-dark.css">
	<link rel="stylesheet" href="/assets/styles.css">

	<!-- Include favicon paths -->
	<?php include "./include_favicons.php" ?>

	<!-- Disable Dark Reader -->
	<meta name="darkreader-lock">
</head>

<script>
	// cross button pressed
	function emptyInput(inputId) {
		// empty error
		document.getElementById('img-size-err').innerHTML = '';
		// empty input val
		document.getElementById(inputId).value = '';
		// update file name indicator
		document.getElementById('selected-file').innerHTML = '<?= $no_file ?>';
		// hide cross button
		document.getElementById('cross-button').hidden = true;
	}

	// input changed
	function validateSize(input) {
		const fileSize = input.files[0].size;
		let displayCross = false;
		if (fileSize > <?= $max_size * 1000_000 ?>) {
			document.getElementById('img-size-err')
				.innerHTML = "Image size exceeds <?= $max_size ?>MB";
			input.value = '';
		} else {
			document.getElementById('img-size-err').innerHTML = '';
			displayCross = Boolean(input.value);
		}

		// show cross button if file is not deleted
		document.getElementById('cross-button').hidden = !displayCross;

		// update file name indicator
		document.getElementById('selected-file').innerHTML = input.files?.[0]?.name ?? '<?= $no_file ?>';
	}
</script>

<body>

	<!-- Include header -->
	<?php include "./header.php" ?>

	<h1>Upload a picture</h1>
	<form action="./send_mail.php" method="POST" enctype="multipart/form-data">
		<label for="code">HTTP Code</label><br>
		<input style='width: 100%; margin-top: 5px;' type="number" id="code" name="code" min="100" max="511" value="<?= $code ?>"><br>
		<div>
			<label for="name">Name</label><br>
			<div class='input-desc'>Image author's name</div>
			<input style='width: 100%' type="text" id="name" name="name" placeholder="DragonLord64"><br>
		</div>
		<div>
			<label for="email">Email</label><br>
			<div class='input-desc'>Email I'll use if I want to contact you</div>
			<input style='width: 100%' type="email" id="email" name="email" placeholder="contact@example.com"><br>
		</div>
		<div>
			<label for="url">Url</label><br>
			<div class='input-desc'>Url that will be shown to credit the author</div>
			<input style='width: 100%' type="text" id="url" name="url" placeholder="https://thedragon.lord.com"><br>
		</div>
		<label for="text">Message</label><br>
		<textarea id="text" name="text"></textarea>
		<label style='margin-top: 15px;' for="image"><span>*</span> Image</label><br>
		<div class='input-desc'>Max <?= $max_size ?>M</div>
		<div>
			<button type='button' onclick='document.getElementById("image").click()'>
				Select Image
			</button>
			<div style='display: flex; align-items: end; gap: 10px;'>
				<i id='selected-file'><?= $no_file ?></i>
				<button hidden id='cross-button' onclick='emptyInput("image")' type='button' style='padding-top: 5px; padding-bottom: 5px; padding-left: 7px; padding-right: 7px; border-radius: 100%;' class='cancel'>
					<img style='height: 12px;' alt='cross icon' src='/assets/icons/cross.png' />
				</button>
			</div>
		</div>
		<input onchange='validateSize(this)' style='display: none' type="file" id="image" name="image" accept="image/*" required>
		<div style='color: red;' id='img-size-err'></div>
		<div style='margin-top: 45px;'>
			<input type="submit" value="Submit Image">
		</div>
	</form>
	<br />
	<div style='color: gold;'>* Mandatory fields</div>
	<div style='margin-top: 10px; font-size: 0.9em'>
		<i>
			The images will be manually reviewed. <br>
			NSFW, Gore and shocking content will not be accepted. <br>
			To keep things accessible for everyone, having text is discouraged. <br>
			Please do not send images without the author's consent! <br>
		</i>
	</div>

	<!-- Include footer html -->
	<?php include "./footer.php" ?>

</body>
