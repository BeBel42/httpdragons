<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>HTTP Status Dragons</title>
	<meta name="description" content="Image of a Dragon for each HTTP Status Code">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/water-dark.css">
	<link rel="stylesheet" href="/assets/styles.css">

	<!-- Include favicon paths -->
	<?php include "./include_favicons.php" ?>

	<!-- Disable Dark Reader -->
	<meta name="darkreader-lock">

	<!-- Facebook Meta Tags -->
	<meta property="og:url" content="https://httpdragons.com">
	<meta property="og:type" content="website">
	<meta property="og:title" content="About HTTP Dragons">
	<meta property="og:description" content="HTTP Status Dragons is where you can upload and see images of a dragon representing every http code!">
	<meta property="og:image" content="https://httpdragons.com/images/example.png">

	<!-- Twitter Meta Tags -->
	<meta name="twitter:card" content="summary_large_image">
	<meta property="twitter:domain" content="httpdragons.com">
	<meta property="twitter:url" content="https://httpdragons.com">
	<meta name="twitter:title" content="About HTTP Dragons">
	<meta name="twitter:description" content="HTTP Status Dragons is where you can upload and see images of a dragon representing every http code!">
	<meta name="twitter:image" content="https://httpdragons.com/images/example.png">

	<!-- Meta Tags Generated via https://www.opengraph.xyz -->
</head>

<body>
	<!--[if lt IE 8]>
			<p class="browserupgrade">
			You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
			</p>
		<![endif]-->

	<!-- Include header -->
	<?php include "./header.php" ?>

<div>
    <p>
        <h2>About HTTP Dragons</h2>
        Hey there! Welcome to <a href='/'>HTTP Dragons</a>,
        where HTTP status codes meet mythical creatures! 🐉
    </p>

    <p>
        <h2>What's HTTP Dragons?</h2>
        HTTP Dragons is my pet project aimed at representing HTTP status
        codes with dragon illustrations.
        It's a fun and creative way to make the web a bit more magical!
    </p>

    <p>
        <h2>API and More Information</h2>
        HTTP Dragons is not just about the illustrations; it's also an API!
        You can retrieve the dragon illustration corresponding to each HTTP status code
        by making requests to the appropriate endpoint. Additionally, you can get more
        detailed information about each HTTP status code in JSON format.

        To learn how to use the API, please visit the <a href='/'>index page</a>.
    </p>

    <p>
        <h2>Contributing Your Art</h2>
        I believe in the power of community creativity.
        If you're an artist, feel free to contribute your own dragon
        illustrations for different HTTP status codes.
        Just head over to the <a href='/src/upload.php'>upload page</a> and
        share your masterpiece with me.
        Don't worry; your privacy is always respected,
        and I'll credit you unless you prefer otherwise.
    </p>

    <p>
        <h2>Privacy and Security</h2>
        Your privacy is paramount. No data is collected from you when you
        visit HTTP Dragons, and any uploads can be done anonymously.
    </p>

    <p>
        <h2>License</h2>
        HTTP Dragons operates under the
        <a href='https://www.gnu.org/licenses/agpl-3.0.en.html'>
            GNU Affero General Public License</a>,
        promoting open collaboration and sharing.
    </p>

    <p>
        <h2>About the Author</h2>
        Interested in knowing more about the person behind HTTP Dragons? Visit my personal website:
        <a href='https://mlefevre.xyz'>mlefevre.xyz</a>.
    </p>

    <p>
        That's HTTP Dragons for you!
        Thanks for being a part of this whimsical adventure where
        dragons and HTTP codes unite! 🐲
    </p>
</div>

<!-- Include footer html -->
<?php include "./footer.php" ?>

</body>
