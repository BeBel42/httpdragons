<?php

require_once './get_codes.php'; # $http_codes is now available
require_once './allow_cors.php';

// Exit if query string is invalid
function exit_error(string $msg, int $code): void
{
	error_log($msg);
	http_response_code($code);
	exit();
}

// img path validation
if (empty($_GET['code'])) {
	exit_error("HTTP Code is empty", 400);
}

// extracting and checking for code
$code = (int)$_GET['code'];
if (!array_key_exists($code, $http_codes)) {
	exit_error("HTTP Code does not exist", 404);
}

// taking json from codes list
$json = $http_codes[$code];

# current url
$actual_link = "https://$_SERVER[HTTP_HOST]";
# adding image url to json
$json['image_url'] = $actual_link . '/' . $code . '.png';

# adding http dragons image url to json
$json['http_dragons_image_url'] = $actual_link . '/src/preview_image.php?code=' . $code;

# adding http code to json response
$json['code'] = $code;

// output type is JSON
header('Content-Type: application/json');
// cache the json for 5 days
header('Cache-Control: public, max-age=432000');
// output json
echo json_encode($json);
