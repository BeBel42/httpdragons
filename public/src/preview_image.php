<?php
require_once './utils.php';
require_once './get_codes.php'; # $http_codes is now available


$code = (int)$_GET['code'];
$index = array_search($code, array_keys($http_codes));
if ($index === false) {
	http_response_code(400);
	error_log("Invalid code: {$_GET['code']}");
	exit();
}
$ht_ob = $http_codes[$code];

$image_url = "/{$code}.png";
$actual_link = 'https://' . "$_SERVER[HTTP_HOST]" . $image_url;

$json_url = "/{$code}.json";
$actual_json_link = 'https://' . "$_SERVER[HTTP_HOST]" . $json_url;

$next_code = get_next_code($code);
$prev_code = get_prev_code($code);

$current_url = strtok($_SERVER["REQUEST_URI"], '?');
$prev_url = ($prev_code === null ? null : $current_url . '?code=' . $prev_code);
$next_url = ($next_code === null ? null : $current_url . '?code=' . $next_code);

function author_section($name, $url)
{
	$name = $name ?? 'Anonymous';
	if ($url) {
		$link_img = 'https://www.google.com/s2/favicons?domain=' . parse_url($url, PHP_URL_HOST);
		$img_html = "<img style='height: 0.9em; width: 0.9em; margin-right: 5px;' src='{$link_img}'>";
		return "<a href='{$url}'>{$img_html} {$name}</a> ";
	}
	return 'By ' . $name;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>HTTP Status Dragons</title>
	<meta name="description" content="Image of a Dragon for each HTTP Status Code">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/water-dark.css">
	<link rel="stylesheet" href="/assets/styles.css">

	<!-- Include favicon paths -->
	<?php include "./include_favicons.php" ?>

	<!-- Disable Dark Reader -->
	<meta name="darkreader-lock">

	<!-- Facebook Meta Tags -->
	<meta property="og:url" content="https://httpdragons.com">
	<meta property="og:type" content="website">
	<meta property="og:title" content=" <?= $code . ' ' . $ht_ob['http_message'] ?>">
	<meta property="og:description" content="HTTP Status Dragons is where you can upload and see images of a dragon representing every http code!">
	<meta property="og:image" content="<?= 'https://httpdragons.com/' . $code . '.png' ?>">

	<!-- Twitter Meta Tags -->
	<meta name="twitter:card" content="summary_large_image">
	<meta property="twitter:domain" content="httpdragons.com">
	<meta property="twitter:url" content="https://httpdragons.com">
	<meta property="twitter:title" content=" <?= $code . ' ' . $ht_ob['http_message'] ?>">
	<meta name="twitter:description" content="HTTP Status Dragons is where you can upload and see images of a dragon representing every http code!">
	<meta property="twitter:image" content="<?= 'https://httpdragons.com/' . $code . '.png' ?>">

	<!-- Meta Tags Generated via https://www.opengraph.xyz -->
</head>

<body>
	<!--[if lt IE 8]>
			<p class="browserupgrade">
			You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
			</p>
		<![endif]-->

	<!-- Include header -->
	<?php include "./header.php" ?>

	<div class='preview-img-layout'>
		<div class='preview-img-img-layout'>
			<div class='pictures'>
				<div style='margin: 0;'>
					<a href="<?= $image_url ?>">
						<div class='border-hover img-container'>
							<img alt="Image for <?= $code ?> <?= $ht_ob['http_message'] ?>" onload='this.style.height = ""; this.style.width = "";' style='max-height: 500px; height: min(500px, 80vw); width: min(500px, 80vw);' class='img-big' src='<?= "/" . $code . ".png?h=500" ?>' />
						</div>
					</a>
					<div class='picture-title'><?= $code ?></div>
					<div> <?= $ht_ob['http_message'] ?> </div>
				</div>
			</div>
		</div>

		<div class='preview-desc-layout'>
			<h1>
				<span style='color: gold'><?= $code ?></span>
				<span><?= $ht_ob['http_message'] ?></span>
			</h1>
			<!-- JSON / img links -->
			<h3>Image Link</h3>
			<a style='margin-bottom: 5px; display: flex; justify-content: left;' href="<?= $image_url ?>"> <?= $actual_link ?> </a>
			<h3>JSON Link</h3>
			<a style='margin-bottom: 5px; display: flex; justify-content: left;' href="<?= $json_url ?>"> <?= $actual_json_link ?> </a>

			<h3>Image Author</h3>
			<div>
				<?= author_section($ht_ob['author_name'], $ht_ob['author_url']); ?>
			</div>

			<h3>Description</h3>
			<div>
				<?= $ht_ob['description'] ?>

			</div>
			<div style='margin-top: auto;'>
				<a href='https://http.dev/<?= $code ?>'>More info</a>
			</div>
		</div>

	</div>

	<!-- Next / Prev buttons -->
	<div style='margin-top: 60px; margin-bottom: 15px; display: flex; justify-content: space-between;'>
		<?php if ($prev_code !== null) : ?>
			<div style='margin-left: 20px'>
				<a href='<?= $prev_url ?>' class='preview-link-layout-left'>
					<div>
						<?= htmlspecialchars('<') ?>
						<?= $prev_code ?>
						<?= $http_codes[$prev_code]['http_message'] ?>
					</div>
					<img alt="Previous image" src='/<?= $prev_code ?>.png?h=30' style='margin-bottom: 10px; margin-top: 10px; margin-left: 15px;'>
				</a>
			</div>
		<?php else : ?>
			<div></div>
		<?php endif; ?>
		<?php if ($next_code !== null) : ?>
			<div style='margin-right: 20px;'>
				<a href='<?= $next_url ?>' class='preview-link-layout-right'>
					<img alt="Next image" height='30' src='/<?= $next_code ?>.png?h=30' style='margin-bottom: 10px; margin-top: 10px; margin-right: 15px;'>
					<div>
						<?= $next_code ?>
						<?= $http_codes[$next_code]['http_message'] ?>
						<?= htmlspecialchars('>') ?>
					</div>
				</a>
			</div>
		<?php else : ?>
			<div></div>
		<?php endif; ?>
	</div>

	<!-- Include footer html -->
	<?php include "./footer.php" ?>

</body>
</div>

</html>
