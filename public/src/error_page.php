<?php

/*
  This page is an example template for using http dragons
*/

require_once './get_codes.php'; # $http_codes is now available

// get code from query string
$code = 0;
if (!empty($_GET['code'])) {
	$code = (int)$_GET['code'];
}

// stop if error code is invalid
if (!array_key_exists($code, $http_codes)) {
	echo('Error: code ' . $code . ' is not a valid code');
	exit(1);
}

$ht_ob = $http_codes[$code];

// http code description
$desc = $ht_ob['http_message'];

// image urls and params
$img_height = 500;
$img_full = "https://httpdragons.com/" . $code . '.png';
$img_prev = $img_full . '?h=' . $img_height;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="<?= $code ?> <?= $desc ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?= $code ?> <?= $desc ?></title>

	<!-- Include favicon paths - delete it if you don't need it-->
	<?php include "./include_favicons.php" ?>

	<!-- Importing water.css to change default styles -->
	<!-- You can import water.css via CDN if you prefer:
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css"> -->
	<link rel="stylesheet" href="/assets/water-dark.css">
	<link rel="stylesheet" href="/assets/styles.css">

	<!-- Disable Dark Reader -->
	<meta name="darkreader-lock">
</head>

<body>

	<div style='display: flex;
				flex-direction: column;
				text-align: center;
				align-items: center
				'>
		<div style='
					background-color: #171717;
					border-radius: 20px;
					padding: 50px;
					'>
			<a href='https://httpdragons.com/src/preview_image.php?code=<?= $code ?>'>
				<img height="<?= $img_height ?>" src='<?= $img_prev ?>' />
			</a>
			<h1 style='margin: 0; margin-top: 25px;'>
				<span style='color: gold;'><?= $code ?></span> <span><?= $desc ?></span>
			</h1>
		</div>
		<div style='margin-top: 40px;'>
			<a href='/' style='
					 background-color: #000;
					 padding: 12px;
					 border-radius: 7px;
					 '>Return Home</a>
		</div>
		<div style='margin-top: 60px;'>
			Image from <a href='https://httpdragons.com'>HTTP Dragons</a>
		</div>
	</div>

	<!-- Include footer html -->
	<?php include "./footer.php" ?>

</body>
</div>

</html>
