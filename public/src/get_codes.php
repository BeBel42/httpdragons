<?php

$http_codes = array(
	100 =>
	array(
		'http_message' => 'Continue',
		'author_name' => 'MadArgon',
		'author_url' => 'https://www.artstation.com/artwork/P6e0An',
		'description' => 'This interim response indicates that the client should continue the request or ignore the response if the request is already finished.',
	),
	101 =>
	array(
		'http_message' => 'Switching Protocols',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This code is sent in response to an Upgrade request header from the client and indicates the protocol the server is switching to.',
	),
	102 =>
	array(
		'http_message' => 'Processing',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This code indicates that the server has received and is processing the request, but no response is available yet.',
	),
	103 =>
	array(
		'http_message' => 'Early Hints',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'This status code is primarily intended to be used with the Link header, letting the user agent start preloading resources while the server prepares a response or preconnect to an origin from which the page will need resources.',
	),
	200 =>
	array(
		'http_message' => 'OK',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The request succeeded. The result meaning of "success" depends on the HTTP method:GET: The resource has been fetched and transmitted in the message body.
HEAD: The representation headers are included in the response without any message body.
PUT or POST: The resource describing the result of the action is transmitted in the message body.
TRACE: The message body contains the request message as received by the server.',
	),
	201 =>
	array(
		'http_message' => 'Created',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/An-Artist-1017707147',
		'description' => 'The request succeeded, and a new resource was created as a result. This is typically the response sent after POST requests, or some PUT requests.',
	),
	202 =>
	array(
		'http_message' => 'Accepted',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The request has been received but not yet acted upon.
It is noncommittal, since there is no way in HTTP to later send an asynchronous response indicating the outcome of the request.
It is intended for cases where another process or server handles the request, or for batch processing.',
	),
	203 =>
	array(
		'http_message' => 'Non-Authoritative Information',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Mirror-Image-1041558379',
		'description' => 'This response code means the returned metadata is not exactly the same as is available from the origin server, but is collected from a local or a third-party copy.
This is mostly used for mirrors or backups of another resource.
Except for that specific case, the 200 OK response is preferred to this status.',
	),
	204 =>
	array(
		'http_message' => 'No Content',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'There is no content to send for this request, but the headers may be useful.
The user agent may update its cached headers for this resource with the new ones.',
	),
	205 =>
	array(
		'http_message' => 'Reset Content',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Tells the user agent to reset the document which sent this request.',
	),
	206 =>
	array(
		'http_message' => 'Partial Content',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This response code is used when the Range header is sent from the client to request only part of a resource.',
	),
	207 =>
	array(
		'http_message' => 'Multi-Status',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Conveys information about multiple resources, for situations where multiple status codes might be appropriate.',
	),
	208 =>
	array(
		'http_message' => 'Already Reported',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Used inside a <dav:propstat> response element to avoid repeatedly enumerating the internal members of multiple bindings to the same collection.',
	),
	218 =>
	array(
		'http_message' => 'This Is Fine',
		'author_name' => 'Snowstorm',
		'author_url' => 'https://www.deviantart.com/snowstorm426',
		'description' => 'HTTP response status code 218 This is fine is an unofficial HTTP response that is used specifically by the Apache Web Server. It is used as a catch-all error condition that allows the passage of a message body through Apache when the ProxyErrorOverride setting is enabled.',
	),
	226 =>
	array(
		'http_message' => 'IM Used',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The server has fulfilled a GET request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.',
	),
	300 =>
	array(
		'http_message' => 'Multiple Choices',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The request has more than one possible response. The user agent or user should choose one of them. (There is no standardized way of choosing one of the responses, but HTML links to the possibilities are recommended so the user can pick.)',
	),
	301 =>
	array(
		'http_message' => 'Moved Permanently',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The URL of the requested resource has been changed permanently. The new URL is given in the response.',
	),
	302 =>
	array(
		'http_message' => 'Found',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Found-1008508853',
		'description' => 'This response code means that the URI of requested resource has been changed temporarily.
Further changes in the URI might be made in the future. Therefore, this same URI should be used by the client in future requests.',
	),
	303 =>
	array(
		'http_message' => 'See Other',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The server sent this response to direct the client to get the requested resource at another URI with a GET request.',
	),
	304 =>
	array(
		'http_message' => 'Not Modified',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This is used for caching purposes.
It tells the client that the response has not been modified, so the client can continue to use the same cached version of the response.',
	),
	305 =>
	array(
		'http_message' => 'Use Proxy',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Defined in a previous version of the HTTP specification to indicate that a requested response must be accessed by a proxy.
It has been deprecated due to security concerns regarding in-band configuration of a proxy.',
	),
	306 =>
	array(
		'http_message' => 'Switch Proxy',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This code was used in a previous version of the HTTP specification to indicate that subsequent requests should use the specified proxy.
It is no longer used and has been reserved for future use',
	),
	307 =>
	array(
		'http_message' => 'Temporary Redirect',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The server sends this response to direct the client to get the requested resource at another URI with the same method that was used in the prior request.
This has the same semantics as the 302 Found HTTP response code, with the exception that the user agent must not change the HTTP method used: if a POST was used in the first request, a POST must be used in the second request.',
	),
	308 =>
	array(
		'http_message' => 'Permanent Redirect',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This means that the resource is now permanently located at another URI, specified by the Location: HTTP Response header.
This has the same semantics as the 301 Moved Permanently HTTP response code, with the exception that the user agent must not change the HTTP method used: if a POST was used in the first request, a POST must be used in the second request.',
	),
	400 =>
	array(
		'http_message' => 'Bad Request',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).',
	),
	401 =>
	array(
		'http_message' => 'Unauthorized',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Although the HTTP standard specifies "unauthorized", semantically this response means "unauthenticated".
That is, the client must authenticate itself to get the requested response.',
	),
	402 =>
	array(
		'http_message' => 'Payment Required',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'This response code is reserved for future use.
The initial aim for creating this code was using it for digital payment systems, however this status code is used very rarely and no standard convention exists.',
	),
	403 =>
	array(
		'http_message' => 'Forbidden',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Forbidden-1010152242',
		'description' => 'The client does not have access rights to the content; that is, it is unauthorized, so the server is refusing to give the requested resource.
Unlike 401 Unauthorized, the client\'s identity is known to the server.',
	),
	404 =>
	array(
		'http_message' => 'Not Found',
		'author_name' => 'the-childofflames',
		'author_url' => 'https://thechildofflames.carrd.co/',
		'description' => 'The server cannot find the requested resource.
In the browser, this means the URL is not recognized.
In an API, this can also mean that the endpoint is valid but the resource itself does not exist.
Servers may also send this response instead of 403 Forbidden to hide the existence of a resource from an unauthorized client.
This response code is probably the most well known due to its frequent occurrence on the web.',
	),
	405 =>
	array(
		'http_message' => 'Method Not Allowed',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Cornered-1049120327',
		'description' => 'The request method is known by the server but is not supported by the target resource.
For example, an API may not allow calling DELETE to remove a resource.',
	),
	406 =>
	array(
		'http_message' => 'Not Acceptable',
		'author_name' => 'MadArgon',
		'author_url' => 'https://www.artstation.com/artwork/YGenLY',
		'description' => 'This response is sent when the web server, after performing server-driven content negotiation, doesn\'t find any content that conforms to the criteria given by the user agent.',
	),
	407 =>
	array(
		'http_message' => 'Proxy Authentication Required',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This is similar to 401 Unauthorized but authentication is needed to be done by a proxy.',
	),
	408 =>
	array(
		'http_message' => 'Request Timeout',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This response is sent on an idle connection by some servers, even without any previous request by the client.
It means that the server would like to shut down this unused connection.
This response is used much more since some browsers, like Chrome, Firefox 27+, or IE9, use HTTP pre-connection mechanisms to speed up surfing.
Also note that some servers merely shut down the connection without sending this message.',
	),
	409 =>
	array(
		'http_message' => 'Conflict',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Conflict-1008079692',
		'description' => 'This response is sent when a request conflicts with the current state of the server.',
	),
	410 =>
	array(
		'http_message' => 'Gone',
		'author_name' => 'DragonArt-Z',
		'author_url' => 'https://www.deviantart.com/dragonart-z',
		'description' => 'This response is sent when the requested content has been permanently deleted from server, with no forwarding address.
Clients are expected to remove their caches and links to the resource.
The HTTP specification intends this status code to be used for "limited-time, promotional services".
APIs should not feel compelled to indicate resources that have been deleted with this status code.',
	),
	411 =>
	array(
		'http_message' => 'Length Required',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Server rejected the request because the Content-Length header field is not defined and the server requires it.',
	),
	412 =>
	array(
		'http_message' => 'Precondition Failed',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The client has indicated preconditions in its headers which the server does not meet.',
	),
	413 =>
	array(
		'http_message' => 'Payload Too Large',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Request entity is larger than limits defined by server.
The server might close the connection or return an Retry-After header field.',
	),
	414 =>
	array(
		'http_message' => 'URI Too Long',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The URI requested by the client is longer than the server is willing to interpret.',
	),
	415 =>
	array(
		'http_message' => 'Unsupported Media Type',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Contract-1049119525',
		'description' => 'The media format of the requested data is not supported by the server, so the server is rejecting the request.',
	),
	416 =>
	array(
		'http_message' => 'Range Not Satisfiable',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The range specified by the Range header field in the request cannot be fulfilled.
It\'s possible that the range is outside the size of the target URI\'s data.',
	),
	417 =>
	array(
		'http_message' => 'Expectation Failed',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'This response code means the expectation indicated by the Expect request header field cannot be met by the server.',
	),
	418 =>
	array(
		'http_message' => 'I\'m a teapot',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The server refuses the attempt to brew coffee with a teapot.',
	),
	419 =>
	array(
		'http_message' => 'Page Expired',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'HTTP response status code 419 Page Expired is an unofficial client error that is Laravel-specific and returned by the server to indicate that the Cross-Site Request Forgery (CSRF) validation has failed.',
	),
	420 =>
	array(
		'http_message' => 'Enhance Your Calm',
		'author_name' => 'Daieny',
		'author_url' => 'https://www.furaffinity.net/user/daieny/',
		'description' => 'The HTTP response status code 420 Method Failure and 420 Enhance your calm is an unofficial client error that is returned by the server to indicate a client error. If it is sent by the Spring Framework then it indicates that a method has failed, whereas if it is sent by Twitter then it indicates that the client is being rate limited for making too many requests.',
	),
	421 =>
	array(
		'http_message' => 'Misdirected Request',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The request was directed at a server that is not able to produce a response.
This can be sent by a server that is not configured to produce responses for the combination of scheme and authority that are included in the request URI.',
	),
	422 =>
	array(
		'http_message' => 'Unprocessable Entity',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Unprocessable-Entity-1016696717',
		'description' => 'The request was well-formed but was unable to be followed due to semantic errors.',
	),
	423 =>
	array(
		'http_message' => 'Locked',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Fish-in-a-birdcage-1007431023',
		'description' => 'The resource that is being accessed is locked.',
	),
	424 =>
	array(
		'http_message' => 'Failed Dependency',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The request failed due to failure of a previous request.',
	),
	425 =>
	array(
		'http_message' => 'Too Early',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Indicates that the server is unwilling to risk processing a request that might be replayed.',
	),
	426 =>
	array(
		'http_message' => 'Upgrade Required',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Thistle-1009289664',
		'description' => 'The server refuses to perform the request using the current protocol but might be willing to do so after the client upgrades to a different protocol.
The server sends an Upgrade header in a 426 response to indicate the required protocol(s).',
	),
	428 =>
	array(
		'http_message' => 'Precondition Required',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The origin server requires the request to be conditional.
This response is intended to prevent the \'lost update\' problem, where a client GETs a resource\'s state, modifies it and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.',
	),
	429 =>
	array(
		'http_message' => 'Too Many Requests',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The user has sent too many requests in a given amount of time ("rate limiting").',
	),
	431 =>
	array(
		'http_message' => 'Request Header Fields Too Large',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The server is unwilling to process the request because its header fields are too large.
The request may be resubmitted after reducing the size of the request header fields.',
	),
	451 =>
	array(
		'http_message' => 'Unavailable For Legal Reasons',
		'author_name' => 'KenyaJoy',
		'author_url' => 'https://www.deviantart.com/kenyajoy',
		'description' => 'The user agent requested a resource that cannot legally be provided, such as a web page censored by a government.',
	),
	500 =>
	array(
		'http_message' => 'Internal Server Error',
		'author_name' => 'Jekyre',
		'author_url' => 'https://bsky.app/profile/jekyre.bsky.social/post/3ldlq76tzic2e',
		'description' => 'The server has encountered a situation it does not know how to handle.',
	),
	501 =>
	array(
		'http_message' => 'Not Implemented',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The request method is not supported by the server and cannot be handled. The only methods that servers are required to support (and therefore that must not return this code) are GET and HEAD.',
	),
	502 =>
	array(
		'http_message' => 'Bad Gateway',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'This error response means that the server, while working as a gateway to get a response needed to handle the request, got an invalid response.',
	),
	503 =>
	array(
		'http_message' => 'Service Unavailable',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/Well-there-goes-my-day-plans-1018856780',
		'description' => 'The server is not ready to handle the request.
Common causes are a server that is down for maintenance or that is overloaded.
Note that together with this response, a user-friendly page explaining the problem should be sent.
This response should be used for temporary conditions and the Retry-After HTTP header should, if possible, contain the estimated time before the recovery of the service.
The webmaster must also take care about the caching-related headers that are sent along with this response, as these temporary condition responses should usually not be cached.',
	),
	504 =>
	array(
		'http_message' => 'Gateway Timeout',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73/art/1060632806',
		'description' => 'This error response is given when the server is acting as a gateway and cannot get a response in time.',
	),
	505 =>
	array(
		'http_message' => 'HTTP Version Not Supported',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The HTTP version used in the request is not supported by the server.',
	),
	506 =>
	array(
		'http_message' => 'Variant Also Negotiates',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'The server has an internal configuration error: the chosen variant resource is configured to engage in transparent content negotiation itself, and is therefore not a proper end point in the negotiation process.',
	),
	507 =>
	array(
		'http_message' => 'Insufficient Storage',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The method could not be performed on the resource because the server is unable to store the representation needed to successfully complete the request.',
	),
	508 =>
	array(
		'http_message' => 'Loop Detected',
		'author_name' => 'Spiritpaw73',
		'author_url' => 'https://www.deviantart.com/spiritpaw73',
		'description' => 'The server detected an infinite loop while processing the request.',
	),
	510 =>
	array(
		'http_message' => 'Not Extended',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Further extensions to the request are required for the server to fulfill it.',
	),
	511 =>
	array(
		'http_message' => 'Network Authentication Required',
		'author_name' => NULL,
		'author_url' => NULL,
		'description' => 'Indicates that the client needs to authenticate to gain network access.',
	),
);
