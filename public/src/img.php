<?php

require_once './allow_cors.php';

// Exit if query string is invalid
function exit_error(string $msg, int $code): void
{
	error_log($msg);
	http_response_code($code);
	exit();
}

// img path validation
if (!isset($_GET['path']) || $_GET['path'] === '') {
	exit_error("PNG Path is empty", 400);
}

// building absolute img path
$img_uri = $_GET['path'];
$public_path = $_SERVER['DOCUMENT_ROOT'];
$path = $public_path . '/images/' . basename($img_uri); // images are in /images

// zero for auto
$w = 0;
$h = 0;

// getting values from query string
if (!empty($_GET['w'])) {
	$w = (int)filter_var($_GET['w'], FILTER_SANITIZE_NUMBER_INT);
	$w = max(0, $w);
}
if (!empty($_GET['h'])) {
	$h = (int)filter_var($_GET['h'], FILTER_SANITIZE_NUMBER_INT);
	$h = max(0, $h);
}

// creating image from path
$img = imagecreatefrompng($path);
if ($img === false) {
	exit_error('image at path is not a valid png', 404);
}

// to avoid having image bigger than original
$w = min($w, imagesx($img));
$h = min($h, imagesy($img));

// giessing auto values to something reasonable
if ($w === 0 && $h === 0) {
	$w = imagesx($img);
	$h = imagesy($img);
} elseif ($w === 0) {
	$w = (int)(imagesx($img) * ($h / imagesy($img)));
} elseif ($h === 0) {
	$h = (int)(imagesy($img) * ($w / imagesx($img)));
}

// creating new resized image
$resized_img = imagecreatetruecolor($w, $h);
// enabling transparency
imagealphablending($resized_img, false);
imagesavealpha($resized_img, true);
// copy img to resized img
imagecopyresampled(
	$resized_img, $img, 0, 0, 0, 0, $w, $h, imagesx($img), imagesy($img));

// output type is PNG
header('Content-Type: image/png');
// cache the image for 5 days
header('Cache-Control: public, max-age=432000');
// output image binary
imagepng($resized_img);
