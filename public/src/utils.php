<?php

require_once './get_codes.php'; # $http_codes is now available

// check if code.png exists in images folder
function does_exist(int $code): bool
{
	// building absolute img path
	$public_path = $_SERVER['DOCUMENT_ROOT'];
	// images are in /images
	$path = $public_path . '/images/' . $code . '.png';

	return file_exists($path) && is_readable($path);
}

function get_next_code(int $code): int|null
{
	global $http_codes;
	foreach ($http_codes as $key => $value) {
		if ($key > $code && does_exist($key)) return $key;
	}
	return null;
}
function get_prev_code(int $code): int|null
{
	global $http_codes;
	foreach (array_reverse($http_codes, true) as $key => $value) {
		if ($key < $code && does_exist($key)) return $key;
	}
	return null;
}
