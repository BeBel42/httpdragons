# ![httpdragons icon](https://gitlab.com/BeBel42/httpdragons/-/raw/main/media/dragon-icon-transparent.png) httpdragons

[httpdragons.com](https://httpdragons.com)  

Website API to see and upload images of dragons for each http code.  
This site was made as a student project, to put my knowledge of the PHP programming language into practice  

## Preview
![Screenshot of home page](https://gitlab.com/BeBel42/httpdragons/-/raw/main/media/sc1.png)
![Screenshot of home page](https://gitlab.com/BeBel42/httpdragons/-/raw/main/media/sc2.png)

## Deployment
### Secrets
You'll need a proper .env file at the root of the repository for the mailing system to work:
``` shell
SMTP_USER=***
SMTP_PASSWORD=***
MAINTAINER_EMAIL=***
MAINTAINER_NAME=***
```
### Docker
Once the secrets are set up, you can launch the application using `docker-compose`:
``` shell
# app will listen to port 8080
docker-compose up
```
