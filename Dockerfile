FROM php:8.2-fpm-bookworm

# update + install nginx
RUN apt-get update && apt-get install -y nginx

# installing composer and its dependencies
RUN apt-get install -y curl git
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php \
	&& php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# moving files to corresponding directories
WORKDIR /var/www/
COPY . .
RUN mv ./nginx-server.conf /etc/nginx/nginx.conf \
	&& mv public/* html/ \
	&& mv ./php-fpm.conf /usr/local/etc/php-fpm.d/php-fpm.conf

# install php libraries
RUN composer update
RUN	composer install

# Install GD and JPEG support
RUN apt-get install -y zlib1g-dev libpng-dev \
	&& docker-php-ext-install -j$(nproc) gd

# Set up permissions
RUN chown -R www-data:www-data /var/www

EXPOSE 8080

# laucnh php and nginx
CMD php-fpm -y /usr/local/etc/php-fpm.d/php-fpm.conf -D \
	&& nginx -g 'daemon off;'
